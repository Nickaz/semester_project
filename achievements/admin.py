from django.contrib import admin
from .models import Achievements, AchievementsList
# Register your models here.

admin.site.register(AchievementsList)
admin.site.register(Achievements)
