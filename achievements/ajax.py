from django.http import HttpResponse
from django.shortcuts import render

from achievements.forms import AchievementsForm, AchievementsEditForm
from achievements.models import AchievementsList, Achievements

from groups.models import Students


def addAchievement(request):
    print(request.POST)
    form = AchievementsForm(request.POST, request.FILES)
    if form.is_valid():
        form.save()
        for student in Students.objects.all():
            Achievements.objects.create(student_id=student, achievement_id=AchievementsList.objects.all().last(), status=0)
        return render(request, 'achievementsList.html', {'AchievementsList': AchievementsList.objects.all()})
    else:
        return HttpResponse("error")


def deleteAchievement(request):
    try:
        print(request.POST)
        achievement = AchievementsList.objects.get(id=request.POST['id'])
        achievement.delete()
        return render(request, 'achievementsList.html', {'AchievementsList': AchievementsList.objects.all()})
    except AchievementsList.DoesNotExist:
        return HttpResponse("error")


def editAchievement(request):
    form = AchievementsEditForm(request.POST, request.FILES)
    if len(request.FILES) != 0 and form.is_valid():
        achievement = AchievementsList.objects.get(id=form.cleaned_data['id'])
        achievement.name = form.cleaned_data['new_name']
        achievement.description = form.cleaned_data['new_description']
        achievement.image_path = form.cleaned_data['new_image']
        achievement.save()
        return render(request, 'achievementsList.html', {'AchievementsList': AchievementsList.objects.all()})
    elif len(request.FILES) == 0:
        achievement = AchievementsList.objects.get(id=request.POST['id'])
        achievement.name = request.POST['new_name']
        achievement.description = request.POST['new_description']
        achievement.save()
        return render(request, 'achievementsList.html', {'AchievementsList': AchievementsList.objects.all()})