import json
import os
import uuid
from django.db.models.signals import pre_delete
from django.dispatch.dispatcher import receiver

from django.db import models
from groups.models import Students

import PIL
from PIL import Image
from imagekit.models.fields import ImageSpecField
from imagekit.processors import ResizeToFit, Adjust, ResizeToFill


class AchievementsList(models.Model):
    name = models.CharField(max_length=100, unique=True)
    image_path = models.ImageField(upload_to='images/', max_length=256, blank=True, null=True)
    description = models.TextField()

    def save(self):
        super().save()
        img = Image.open(self.image_path.path)

        if img.height > 64 or img.width > 64:
            output_size = (64, 64)
            img.thumbnail(output_size)
            img.save(self.image_path.path)

@receiver(pre_delete, sender=AchievementsList)
def image_path_delete(sender, instance, **kwargs):
    storage, path = instance.image_path.storage, instance.image_path.path
    storage.delete(path)


class Achievements(models.Model):
    achievement_id = models.ForeignKey(AchievementsList, on_delete=models.CASCADE)
    student_id = models.ForeignKey(Students, on_delete=models.CASCADE)
    status = models.IntegerField()
