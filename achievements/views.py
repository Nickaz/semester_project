from django.http import HttpResponseRedirect, HttpResponseNotFound, JsonResponse
from django.shortcuts import render, HttpResponse
from django.views.generic import ListView, DetailView
import django_forms_bootstrap
from django.views.generic.edit import FormMixin
from django.contrib.auth.mixins import LoginRequiredMixin

from .models import AchievementsList
from .forms import AchievementsForm, AchievementsEditForm


class AchievementsView(LoginRequiredMixin, FormMixin, ListView):
    model = AchievementsList
    template_name = 'achievements.html'
    context_object_name = 'AchievementsList'
    form_class = AchievementsForm
    second_form_class = AchievementsEditForm
    success_url = '/achievements/'

    def get_context_data(self, **kwargs):
        context = super(AchievementsView, self).get_context_data(**kwargs)
        if 'formAppend' not in context:
            context['formAppend'] = self.form_class()
        if 'formEdit' not in context:
            context['formEdit'] = AchievementsEditForm()
        return context

    def get_queryset(self):
        return AchievementsList.objects.all()

    def post(self, request, *args, **kwargs):
        if 'btnform' in request.POST:
            form = AchievementsForm(request.POST, request.FILES)
            form_name = 'formAppend'
        else:
            form = AchievementsEditForm(request.POST, request.FILES)
            if len(request.FILES) != 0 and form.is_valid():
                achievement = AchievementsList.objects.get(id=form.cleaned_data['id'])
                achievement.name = form.cleaned_data['new_name']
                achievement.description = form.cleaned_data['new_description']
                achievement.image_path = form.cleaned_data['new_image']
                achievement.save()
                return HttpResponseRedirect('/achievements/')
            elif len(request.FILES) == 0:
                achievement = AchievementsList.objects.get(id=request.POST['id'])
                achievement.name = request.POST['new_name']
                achievement.description = request.POST['new_description']
                achievement.save()
                return HttpResponseRedirect('/achievements/')

        if form.is_valid():
            return self.form_valid(form)
        else:
            return self.form_invalid(form)

    def form_valid(self, form):
        form.save()
        return super().form_valid(form)


