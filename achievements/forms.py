from django import forms
import django_forms_bootstrap
from .models import AchievementsList


class AchievementsForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        for field in self.Meta.fields_required:
            self.fields[field].required = True

    class Meta:
        model = AchievementsList
        fields = ('name', 'description', 'image_path')
        fields_required = ('name', 'description', 'image_path')


class AchievementsEditForm(forms.Form):
    new_name = forms.CharField(label='Название')
    new_description = forms.CharField(label='Описание', widget=forms.Textarea)
    new_image = forms.ImageField()
    id = forms.IntegerField()
    new_image.required = False
