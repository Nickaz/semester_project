from django.conf.urls import url
from django.urls import path
from achievements import views
from .views import AchievementsView
from achievements.ajax import addAchievement, deleteAchievement, editAchievement

urlpatterns = [
    #path('', achievementsList, name='achievementsList'),
    path('', AchievementsView.as_view(), name='achievementsView'),
    path('deleteAchievement/', deleteAchievement),
    path('addAchievement/', addAchievement, name='addAchievement'),
    path('editAchievement/', editAchievement, name='editAchievement'),
]
