from .base import FunctionalTest
from selenium import webdriver
from selenium.webdriver.common.keys import Keys


class GroupsValidationTest(FunctionalTest):

    def test_can_add_more_than_one_group(self):
        
        # Павел Викторович заходит в раздел "Группы"
        self.browser.get(self.live_server_url + '/groups/')

        self.login()

        self.assertIn('Группы', self.browser.title)
        
        # Он сразу же решает проверить, корректно ли работает список
        # Для этого он добавляет одну группу
        field = self.browser.find_element_by_id('id_name')
        field.send_keys("group1")
        field.send_keys(Keys.ENTER)

        # Она появляется в списке
        self.wait_for_item_in_table('id_group_table', 'group1')

        # Затем вторую
        field = self.browser.find_element_by_id('id_name')
        field.send_keys("group2")
        field.send_keys(Keys.ENTER)

        # Она также появляется в списке
        self.wait_for_item_in_table('id_group_table', 'group2')

        # И третью
        field = self.browser.find_element_by_id('id_name')
        field.send_keys("group3")
        field.send_keys(Keys.ENTER)

        # Теперь он видит в списке все три группы
        self.wait_for_item_in_table('id_group_table', 'group1')
        self.wait_for_item_in_table('id_group_table', 'group2')
        self.wait_for_item_in_table('id_group_table', 'group3')

        # Павел Викторович доволен и идёт дальше охотиться на ошибки

    def test_cannot_create_a_group_without_name(self):
        
        # Павел Викторович заходит в раздел "Группы"
        self.browser.get(self.live_server_url + '/groups/')

        self.login()
        self.assertIn('Группы', self.browser.title)

        # Он видит список групп
        header_text = self.browser.find_element_by_tag_name('h1').text
        self.assertIn('Список групп', header_text)

        # Павел Викторович хочет добавить группу, но сначала решает подловить студентов
        # Он не вводит название и нажимает кнопку "Добавить группу"
        button = self.browser.find_element_by_name('addGroupBtn')
        button.click()

        # На удивление страница браузера не обновляется, а Павел Викторович видит
        # подсказку, которая просит его ввести название
        self.wait_for(lambda: self.browser.find_element_by_css_selector(
            '#id_name:invalid'
            ))

        # Тогда Павел Викторович вписывает название одной из своих любимых групп, ошибка пропадает
        field = self.browser.find_element_by_id('id_name')
        field.send_keys("КИ18-17/2")
        self.wait_for(lambda: self.browser.find_element_by_css_selector(
            '#id_name:valid'
            ))

        # и он снова нажимает "Добавить группу"
        button.click()

        # Теперь группа добавлена и появилась в списке
        self.wait_for_item_in_table('id_group_table', 'КИ18-17/2')

        # На всякий случай, Павел Викторович решает проверить ещё раз
        # он снова не вводит название и нажимает ENTER в поле для ввода
        field = self.browser.find_element_by_id('id_name')
        field.send_keys(Keys.ENTER)

        # Но ничего не происходит, и Павел Викторович снова видит ту же подсказку
        self.wait_for(lambda: self.browser.find_element_by_css_selector(
            '#id_name:invalid'
            ))

        # Довольный, что ничего не сломалось, Павел Викторович ставит всем высший балл!

    def test_cannot_create_duplicate_groups(self):

        # Павел Викторович вспоминает, где могли допустить ошибку студенты
        # Он заходит на сайт в раздел "группы"
        self.browser.get(self.live_server_url + '/groups/')
        self.login()
        self.assertIn('Группы', self.browser.title)

        # Быстро вводит случайное название группы и отправляет
        field = self.browser.find_element_by_id('id_name')  
        field.send_keys("group")
        field.send_keys(Keys.ENTER)

        # Группа как и положено появляется в списке
        self.wait_for_item_in_table('id_group_table', 'group')

        # Наконец, в попытке подловить студентов Павел Викторович 
        # вписывает в поле такое же название группы
        field = self.browser.find_element_by_id('id_name')
        field.send_keys("group")
        field.send_keys(Keys.ENTER)

        # Но ничего не ломается, а Павел Викторович видит подсказку о том,
        # что такая группа уже существует
        self.wait_for(lambda: self.browser.find_element_by_class_name(
            'invalid-feedback'
        ))

        # Павел Викторович в расстерянности, он решает проверить,
        # не сломался ли список. Он снова печатает название другой группы
        field = self.browser.find_element_by_id('id_name')  
        field.clear()
        field.send_keys("another_group")
        field.send_keys(Keys.ENTER)

        # Но группа всё также появляется
        self.wait_for_item_in_table('id_group_table', 'another_group')

        # Не найдя ошибки, Павел Викторович идёт тестить другие места

    def test_can_add_group_with_slashes_and_spaces(self):
        
        # Павел Викторович замечает, что при переходе на страницу группы
        # ссылка подозрительно напоминает название группы, тогда у него созревает план
        # Он понимает, что если в названии будет слэш ('/'), то URL адрес сломается

        # Он быстро заходит на сайт в раздел "группы"
        self.browser.get(self.live_server_url + '/groups/')
        self.login()
        self.assertIn('Группы', self.browser.title)

        # Вводит название группы со слэшем и пробелом
        field = self.browser.find_element_by_id('id_name')  
        field.send_keys("group/has slash")
        field.send_keys(Keys.ENTER)
        
        # К радости Павла Викторовича группа появляется в списке
        self.wait_for_item_in_table('id_group_table', 'group/has slash')

        # Он переходит по ссылке на страницу группы
        group_url = self.browser.find_element_by_link_text('group/has slash')
        group_url.click()

        # Но страница открывается, а имя группы также содержит слэши
        header_text = self.browser.find_element_by_tag_name('h1').text
        self.assertIn('Редактировать группу group/has slash', header_text)

        # Павел Викторович удивлен и продолжает искать ошибки

    def test_can_delete_group(self):
        
        # Павел Викторович снова что-то затеял
        self.browser.get(self.live_server_url + '/groups/')
        self.login()
        self.assertIn('Группы', self.browser.title)

        # Судя по названию группы, которое он ввел, он собирается
        # проверить удаление групп из списка
        field = self.browser.find_element_by_id('id_name')  
        field.send_keys("delete_this")
        field.send_keys(Keys.ENTER)

        # Группа появляется
        self.wait_for_item_in_table('id_group_table', 'delete_this')

        # На всякий случай он добавляет ещё одну группу
        field = self.browser.find_element_by_id('id_name')  
        field.send_keys("save_this")
        field.send_keys(Keys.ENTER)

        # Вторая группа тоже в списке
        self.wait_for_item_in_table('id_group_table', 'save_this')

        # Ему не приходится долго искать способ удаления, ведь при добавлении
        # он сразу замечает красную кнопку с надписью "Удалить группу"
        # Не медля ни секунды он жмёт на неё
        button = None
        rows = self.browser.find_elements_by_tag_name('tr')
        for row in rows:
            tds = row.text.split()
            for td in tds:
                if td == 'delete_this': 
                    button = row.find_element_by_name('deleteBtn')
                    break
            if button is not None:
                break
        button.click()

        # Он подтверждает свой выбор
        self.browser.switch_to_alert().accept()

        # Страница обновляется и удаленная группа исчезает
        self.wait_for(lambda: self.assertNotIn('delete_this',
                     [item.text for item in self.browser.find_elements_by_tag_name('td')]
        ))

        # Однако вторая группа остается
        self.wait_for_item_in_table('id_group_table', 'save_this')

        # "Удаление в порядке" - подумал Павел Викторович и перешёл к следующим проверкам


class StudentsValidationTest(FunctionalTest):

    def test_can_add_more_than_one_students(self):
        # Наконец, Павел Викторович решает заполнить группу студентами
        # Для начала он заходит во вкладку группы и создает группу
        self.browser.get(self.live_server_url + '/groups/')
        self.login()
        field = self.browser.find_element_by_id('id_name')  
        field.send_keys("new_group")
        field.send_keys(Keys.ENTER)
        
        # Группа появляется в таблице
        self.wait_for_item_in_table('id_group_table', 'new_group')

        # Он кликает по добавленной группе
        group_url = self.browser.find_element_by_link_text('new_group')
        group_url.click()

        # Открывается страница со студентами и Павел Викторович видит
        # что список студентов пуст
        header_text = self.browser.find_element_by_tag_name('h1').text
        self.assertIn('Редактировать группу new_group', header_text)
        
        # Он вписывает ФИО студента в поле и нажимает кнопку для добавления
        field = self.browser.find_element_by_id('full_name')  
        field.send_keys("Фамилия Имя Отчество")
        self.browser.find_element_by_name('addStudentButton').click()

        # И студент появляется в таблице
        self.wait_for_item_in_table('id_students_table', 'Фамилия Имя Отчество')

        # Тогда он пробует добавить ещё двух студентов
        field = self.browser.find_element_by_id('full_name')  
        field.send_keys("Ещё один студент")
        self.browser.find_element_by_name('addStudentButton').click()

        # Второй студент появляется в таблице
        self.wait_for_item_in_table('id_students_table', 'Ещё один студент')

        field = self.browser.find_element_by_id('full_name')  
        field.send_keys("третий студент")
        self.browser.find_element_by_name('addStudentButton').click()

        # В итоге все три студента появляются в таблице
        self.wait_for_item_in_table('id_students_table', 'Фамилия Имя Отчество')
        self.wait_for_item_in_table('id_students_table', 'Ещё один студент')
        self.wait_for_item_in_table('id_students_table', 'третий студент')

    def test_cannot_add_student_without_name(self):
        # Павел Викторович с невиданной скоростью открывает раздел групп,
        # создает новую группу, и переходит на неё
        self.browser.get(self.live_server_url + '/groups/')
        self.login()
        field = self.browser.find_element_by_id('id_name')  
        field.send_keys("new_group")
        field.send_keys(Keys.ENTER)
        
        self.wait_for_item_in_table('id_group_table', 'new_group')

        group_url = self.browser.find_element_by_link_text('new_group')
        group_url.click()

        # Там он пытается добавить студента, но не вписывает ничего в поле ФИО
        self.browser.find_element_by_name('addStudentButton').click()

        # Ничего не происходит - всё так и должно быть
        self.assertNotEqual(len(self.browser.find_elements_by_tag_name('tr')), 0)
        
        # По крайней мере, это было быстро

    def test_can_delete_student(self):
        # Павел Викторович решает проверить и удаление студентов
        # Он снова повторяет уже знакомые действия (Создает группу)
        self.browser.get(self.live_server_url + '/groups/')
        self.login()
        field = self.browser.find_element_by_id('id_name')  
        field.send_keys("new_group")
        field.send_keys(Keys.ENTER)
        
        # Группа появляется в таблице
        self.wait_for_item_in_table('id_group_table', 'new_group')

        # Он кликает по добавленной группе, чтобы перейти на её страницу
        group_url = self.browser.find_element_by_link_text('new_group')
        group_url.click()

        # Он вписывает ФИО студента в поле и нажимает кнопку для добавления
        field = self.browser.find_element_by_id('full_name')  
        field.send_keys("Фамилия Имя Отчество")
        self.browser.find_element_by_name('addStudentButton').click()

        # И студент появляется в таблице
        self.wait_for_item_in_table('id_students_table', 'Фамилия Имя Отчество')

        # Тогда он пробует добавить ещё студента
        field = self.browser.find_element_by_id('full_name')  
        field.send_keys("Ещё один студент")
        self.browser.find_element_by_name('addStudentButton').click()

        # Второй студент появляется в таблице
        self.wait_for_item_in_table('id_students_table', 'Ещё один студент')

        # Теперь пора удалить одного из них!
        button = self.browser.find_element_by_name('deleteStudentBtn1')
        button.click()

        # Он подтверждает свой выбор
        self.browser.switch_to_alert().accept()

        # После этого студент сразу же исчезает из списка
        self.wait_for(lambda: self.assertNotIn('Фамилия Имя Отчество',
                     [item.text for item in self.browser.find_elements_by_tag_name('td')]
        ))
        # А второй студент остается там
        self.wait_for_item_in_table('id_students_table', 'Ещё один студент')

        # Павел Викторович и не сомневался, что здесь тоже всё пройдёт гладко
