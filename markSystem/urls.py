from django.contrib import admin
from django.urls import path, include
from groups import urls as groupsUrls
from table import urls as tableUrls
from achievements import urls as achievementsUrls
from table import views as tableViews
from django.conf.urls.static import static
from django.conf import settings

urlpatterns = [
    path('admin/', admin.site.urls),
    path('groups/', include(groupsUrls)),
    path('mainTable/', include(tableUrls)),
    path('', tableViews.homePage, name="homePage"),
    path('achievements/', include(achievementsUrls)),
    path('accounts/', include('django.contrib.auth.urls')),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
