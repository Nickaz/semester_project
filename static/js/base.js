(function() {
  let dismissButton = document.querySelector("#dismiss");
  let overlay = document.querySelector(".overlay");
  let sidebarButton = document.querySelector("#sidebarCollapse");
  let sidebar = document.querySelector("#sidebar");
  let upButton = document.querySelector("#upButton");
  let upButtonDiv = document.querySelector("#upButtonDiv");
  let fillerDiv = document.querySelector("#fillerDiv");
  let scrollNeeded = 60;

  dismissButton.addEventListener("click", function() {
    sidebar.classList.remove("active");
    overlay.classList.remove("active");
  });

  overlay.addEventListener("click", function() {
    sidebar.classList.remove("active");
    overlay.classList.remove("active");
  });

  sidebarButton.addEventListener("click", function() {
    sidebar.classList.add("active");
    overlay.classList.add("active");
  });

  window.onscroll = function() {
    if (
      document.body.scrollTop > scrollNeeded ||
      document.documentElement.scrollTop > scrollNeeded
    ) {
      upButtonDiv.classList.add("active");
      fillerDiv.classList.add("active");
    } else {
      upButtonDiv.classList.remove("active");
      fillerDiv.classList.remove("active");
    }
  };

  upButton.addEventListener("click", function() {
    document.body.scrollTop = 0;
    document.documentElement.scrollTop = 0;
  });
})();
