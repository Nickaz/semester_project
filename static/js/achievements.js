$(document).ready(function() {
    // CSRF code
    function getCookie(name) {
        var cookieValue = null;
        var i = 0;
        if (document.cookie && document.cookie !== '') {
            var cookies = document.cookie.split(';');
            for (i; i < cookies.length; i++) {
                var cookie = jQuery.trim(cookies[i]);
                // Does this cookie string begin with the name we want?
                if (cookie.substring(0, name.length + 1) === (name + '=')) {
                    cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                    break;
                }
            }
        }
        return cookieValue;
    }
    var csrftoken = getCookie('csrftoken');

    function csrfSafeMethod(method) {
        // these HTTP methods do not require CSRF protection
        return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
    }
    $.ajaxSetup({
        crossDomain: false, // obviates need for sameOrigin test
        beforeSend: function(xhr, settings) {
            if (!csrfSafeMethod(settings.type)) {
                xhr.setRequestHeader("X-CSRFToken", csrftoken);
            }
        }
    });

    $('#btnform').on('click', function(e) {
        e.preventDefault();
        var inputs = $('#addForm').find('input');
        var formData = new FormData();
        if (inputs[0].value == "")
        {
            alert('Achievement adding error');
            return;
        } 
        formData.append('name', inputs[0].value);
        formData.append('description', $('#addForm').find('textarea')[0].value);
        formData.append('image_path', $('input[name=image_path]').prop('files')[0]);
        $.ajax({
            url: '/achievements/addAchievement/',
            method: 'POST',
            processData: false,
            contentType: false,
            data: formData,
            success: function(d) {
                $('#achievementsList').html(d)
                alert('Achievement added');
            },
            error: function(d) {
                console.log(d);
                alert('Achievement adding error');
            }
        });
    });

    $('#btnformedit').on('click', function(e) {
        e.preventDefault();
        var inputs = $('#editForm').find('input');
        var formData = new FormData();
        formData.append('new_name', inputs[0].value);
        formData.append('id', inputs[2].value);
        formData.append('new_description', $('#editForm').find('textarea')[0].value);
        formData.append('new_image', $('input[name=new_image]').prop('files')[0]);
        $.ajax({
            url: '/achievements/editAchievement/',
            method: 'POST',
            processData: false,
            contentType: false,
            data: formData,
            success: function(d) {
                $('#achievementsList').html(d)
            },
            error: function(d) {
                console.log(d);
                alert('Achievement editing error');
            }
        });
    });
});


function deleteAchievement(id) {
    if (confirm("Вы действительно хотите удалить достижение?")) {
        $.ajax({
            url: '/achievements/deleteAchievement/',
            method: 'POST',
            data: {
                id:id,
            },
            success: function(d) {
                $('#achievementsList').html(d)
                console.log('success deleting')
            },
            error: function(d) {
                console.log(d);
                alert('Achievement deleting error');
            }
        });
    }
}


function edit_achievement(e, id, achievementName_old, achievementDescription_old, achievementImage_old) {
    var clientRectangle = e.getBoundingClientRect();
    var editForm = $('#editForm');
    editForm.find('input')[0].value = achievementName_old;
    editForm.find('textarea')[0].value = achievementDescription_old;
    editForm.find('input', 'textarea').addClass('empty_field');
    editForm.find('input')[2].style = "display:none";
    editForm.find('input')[2].value = id;
    editForm.find('label')[3].style = "display:none";
    var btn = editForm.find('.btn-primary');

    $('#editAchievementBlock').css({
        display: "block",
        position: "absolute",
        left: clientRectangle.left + "px",
        top: clientRectangle.top + "px",
    });
};

function close_edit_achievement_block() {
    $('#editAchievementBlock').css({
        display: "none",
    });
}