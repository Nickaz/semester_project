function checkDiscipline(){
    let fullNameCells = document.querySelectorAll(".disciplineNameCell");
    let discipline_id = 0;
    for (let i = 0; i < fullNameCells.length; i++) {
        let curCell = fullNameCells[i];
        if (curCell.parentElement.classList.contains("cell-active")) {
            discipline_id = curCell.parentElement.getAttribute('id');
        }
    }
    return discipline_id
}
function updateDisciplineButtons() {
    let disciplineNameCells = document.querySelectorAll(".disciplineNameCell");
    for (let i = 0; i < disciplineNameCells.length; i++) {
        let curDisciplineNameCell = disciplineNameCells[i];
        let discipline_id = curDisciplineNameCell.dataset.discipline;
        curDisciplineNameCell.addEventListener("click", () => chooseDiscipline(discipline_id, curDisciplineNameCell));
    }


    $("#disciplineSubmitButton").on("click", function (e) {
    e.preventDefault();
    $.ajax({
        url: '/mainTable/addDiscipline/',
        method: 'POST',
        data: $('#disciplineCreateForm').serialize(),
        success: function (d) {
            $('#disciplines').html(d);
            updateDisciplineButtons();
        },
        error: function (d) {
            console.log(d);
        }
    });
    });
}
updateDisciplineButtons();

function addTask(e) {
    discipline_id = checkDiscipline();
    if (discipline_id === 0) {
        $('#disciplineTable').css({
            'border': '1.5px solid #d8512d'
        });
        setTimeout(function () {
            $('#disciplineTable').removeAttr('style');
        }, 500);
    } else {
        var left = $('#newTaskBtn').offset().left
        var top = $('#newTaskBtn').offset().top
        $('#taskCreateBlock').css({
            display: "block",
            position: "absolute",
            left: left + "px",
            top: top + "px",
        });
    }
};

function deleteTask(id) {
    let fullNameCells = document.querySelectorAll(".disciplineNameCell");
    let discipline_id = 0;
    for (let i = 0; i < fullNameCells.length; i++) {
        let curCell = fullNameCells[i];
        if (curCell.parentElement.classList.contains("cell-active")) {
            discipline_id = curCell.parentElement.getAttribute('id');
        }
    }
    if (confirm("Вы действительно хотите удалить задание?")) {
        $.ajax({
            url: '/mainTable/deleteTask/',
            method: 'POST',
            data: {
                id:id,
                discipline_id:discipline_id,
            },
            success: function(d) {
                $('#markTableDiv').html(d);
                updateButtons();
                console.log('success deleting')
            },
            error: function(d) {
                console.log(d);
                alert('Task deleting error');
            }
        });
    }
}

function close_block(block) {
    if (block.id == 'close_add_group_btn'){
        $('#addGroupForm').find('input')[0].remove();
        $('#addGroupForm').find('select').remove();
        $('#addGroupBlock').css({
        display: "none",
        });
    } else if (block.id == 'close_create_task_btn'){
        $('#taskCreateBlock').css({
        display: "none",
        });
    } else {
        $('#editMarkBlock').css({
        display: "none"
        })
        $('#editMarkBlock input').val("");
    }
}



function updateButtons() {
    markTable = document.getElementById('markTable');
    for (let i = 1; i < markTable.rows.length; i++) {
        let curRow = markTable.rows[i];
        for (let j = 0; j < curRow.cells.length; j++) {
            let curCell = curRow.cells[j];
            if (curCell.classList.contains("markCell") && !curCell.innerText && curCell.dataset.type === "deadline") {
                createButton(curCell);
            }
        }
    }
    let fullNameCells = document.querySelectorAll(".fullNameCell");
    for (let i = 0; i < fullNameCells.length; i++) {
        let curFullNameCell = fullNameCells[i];
        let student_id = curFullNameCell.dataset.student;
        curFullNameCell.addEventListener("click", () => chooseStudent(student_id, curFullNameCell));
    }

    $(".taskHeader").mousemove(function (eventObject) {
        name = $(this).attr('data-name');
        deadline = $(this).attr('data-deadline');
        $("#tooltip").text(name + '\n' + deadline)
            .css({
                "top": eventObject.pageY + 5,
                "left": eventObject.pageX + 5
            })
            .show();
    }).mouseout(function () {
        $("#tooltip").hide()
            .text("")
            .css({
                "top": 0,
                "left": 0
            });
    });

    $("#addGroup").on("click", function (e) {
        $.ajax({
            url: '/mainTable/getGroupsForDiscipline/',
            method: 'POST',
            data: { discipline_id: $("#addGroup").attr('data-discipline')},
            success: function (selector) {
                $("#addGroupForm").prepend(selector);
                $("#addGroupForm").find('input')[0].value = $("#addGroup").attr('data-discipline');
                $("#addGroupForm").find('input')[0].style = 'display:none;'
            },
            error: function (d) {
                $("#group_adding_error").show();
                $("#close_add_group_btn").hide();
                $("#addGroupSubmitButton").hide();
                $("#addGroupBlock").fadeIn(3000);
                $("#addGroupBlock").fadeOut(3000);
                $("#group_adding_error").delay(3000).fadeOut(1);
                $("#addGroupSubmitButton").delay(3000).fadeIn(1);
                $("#close_add_group_btn").delay(3000).fadeIn(1);
            },
        });
        var left = $('#addGroup').offset().left;
        var top = $('#addGroup').offset().top;
        $('#addGroupBlock').css({
            display: "block",
            position: "absolute",
            left: left + "px",
            top: top + "px",
        });
    });
}

$("#addGroupSubmitButton").on("click", function (e) {
        e.preventDefault();
        $.ajax({
            url: '/mainTable/addGroupToDiscipline/',
            method: 'POST',
            data: $("#addGroupForm").serialize(),
            success: function (table) {
                $('#markTableDiv').html(table);
                updateButtons();
                $("#close_add_group_btn").click();
            },
            error: function (d) {
                console.log(d);
            },
        });
});

function createButton(curCell) {
    let btn = document.createElement("button");
    btn.classList.add("btn", "btn-primary", "setMarkBtn");
    btn.innerHTML = "&check;";
    curCell.appendChild(btn);
    btn.addEventListener("click", () => setMark(curCell.dataset.cell_task, curCell.dataset.cell_student, curCell));
}

function getCookie(name) {
    var cookieValue = null;
    var i = 0;
    if (document.cookie && document.cookie !== '') {
        var cookies = document.cookie.split(';');
        for (i; i < cookies.length; i++) {
            var cookie = jQuery.trim(cookies[i]);
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) === (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}
var csrftoken = getCookie('csrftoken');

function csrfSafeMethod(method) {
    // these HTTP methods do not require CSRF protection
    return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
}
$.ajaxSetup({
    crossDomain: false, // obviates need for sameOrigin test
    beforeSend: function (xhr, settings) {
        if (!csrfSafeMethod(settings.type)) {
            xhr.setRequestHeader("X-CSRFToken", csrftoken);
        }
    }
});

function createInput(task_id, student_id) {
    let input = document.createElement("input");
    input.classList.add("knowledgeMarkInput");
    input.id = `input_${task_id}_${student_id}`;
    input.dataset.input_task = task_id;
    input.dataset.input_student = student_id;
    input.type = "number";
    input.min = "1";
    input.max = "5";
    input.addEventListener("keydown", setKnowledgeMark)
    return input;
}

function setKnowledgeMark(event) {
    let curInput = event.target;
    let task_id = curInput.dataset.input_task;
    let student_id = curInput.dataset.input_student;
    if (event.keyCode === 13 && curInput.value >= 1 && curInput.value <= 5) {
        let cell = curInput.parentElement;
        let mark = curInput.value;

        $.ajax({
            url: '/mainTable/addKnowledgeMark/',
            method: 'POST',
            data: {
                task_id: task_id,
                student_id: student_id,
                mark: mark,
            },
            success: function (d) {
                const data = JSON.parse(d);
                cell.querySelector("input").remove();
                let span = createSpan(task_id, student_id, data['knowledgeMark']);
                let div = createEditDeadlineBtn(task_id, student_id);
                cell.appendChild(span);
                cell.appendChild(div);
                $("#knAvgCell_" + student_id).html(data['avgKnowledgeMark']);
            },
            error: function (d) {
                console.log(d);
            }
        })
    }
}

function createSpan(task_id, student_id, d) {
    let span = document.createElement("span");
    span.id = `span_${task_id}_${student_id}`;
    span.innerHTML = d;
    return span;
}

function createEditDeadlineBtn(task_id, student_id) {
    let div = document.createElement("div");
    div.classList.add("edit-mark-div");
    let btn = document.createElement("button");
    btn.classList.add("edit-mark-btn");
    btn.ariaLabel = "Edit";
    btn.onclick = openChangeBlock;
    btn.dataset.btn_task = task_id;
    btn.dataset.btn_student = student_id;
    let span = document.createElement("span");
    span.ariaHidden = "true";
    span.innerHTML = "&#9998;";
    btn.appendChild(span);
    div.appendChild(btn);
    return div;
}

function setMark(task_id, student_id, curCell) {
    $.ajax({
        url: '/mainTable/addMark/',
        method: 'POST',
        data: {
            task_id: task_id,
            student_id: student_id,
        },
        success: function (d) {
            curCell.lastChild.remove();
            const data = JSON.parse(d);
            let span = createSpan(task_id, student_id, data['deadlineMark']);
            let div = createEditDeadlineBtn(task_id, student_id);
            curCell.appendChild(span);
            curCell.appendChild(div);
            let newInput = createInput(task_id, student_id);
            let td = curCell.nextElementSibling;
            td.appendChild(newInput);
            $("#dlAvgCell_" + student_id).html(data['avgDeadlineMark']);
        },
        error: function (d) {
            console.log(d);
        }
    });
}

$("#taskSubmitButton").on("click", function (e) {
    e.preventDefault();
    let fullNameCells = document.querySelectorAll(".disciplineNameCell");
    let discipline_id = 0;
    for (let i = 0; i < fullNameCells.length; i++) {
        let curCell = fullNameCells[i];
        if (curCell.parentElement.classList.contains("cell-active")) {
            discipline_id = curCell.parentElement.getAttribute('id');
        }
    }
    var inputs = $('#taskCreateForm').find('input');
    var selects = $('#taskCreateForm').find('select');
    let name = inputs[0].value;
    let month = selects[0].value;
    let day = selects[1].value;
    let year = selects[2].value;
    if (name != '') {
        $.ajax({
            url: '/mainTable/addTask/',
            method: 'POST',
            data: {
                name: name,
                deadline_month: month,
                deadline_day: day,
                deadline_year: year,
                discipline_id: discipline_id,
            },
            success: function (d) {
                $('#markTableDiv').html(d);
                updateButtons();
            },
            error: function (d) {
                console.log(d);
            }
        });
    } else {
        inputs.css({
            'border-color': '#d8512d'
        });
        setTimeout(function () {
            inputs.removeAttr('style');
        }, 500);
    }
});

function chooseStudent(student_id, cell) {
    let fullNameCells = document.querySelectorAll(".fullNameCell");
    for (let i = 0; i < fullNameCells.length; i++) {
        let curCell = fullNameCells[i];
        if (curCell.id != cell.id) {
            curCell.parentElement.classList.remove("cell-active");
        }
        else {
            curCell.parentElement.classList.add("cell-active");
        }
    }
    $.ajax({
        url: '/mainTable/studentAchievements/',
        method: 'POST',
        data: {
            student_id: student_id,
        },
        success: function (d) {
            $('#studentAchievementsBar').html(d);
        },
        error: function (d) {
            console.log(d);
        }
    });
}

function chooseDiscipline(discipline_id, cell) {
    let disciplineNameCells = document.querySelectorAll(".disciplineNameCell");
    for (let i = 0; i < disciplineNameCells.length; i++) {
        let curCell = disciplineNameCells[i];
        if (curCell.id != cell.id) {
            curCell.parentElement.classList.remove("cell-active");
        }
        else {
            curCell.parentElement.classList.add("cell-active");
        }
    }
    $.ajax({
        url: '/mainTable/chooseDiscipline/',
        method: 'POST',
        data: {
            discipline_id: discipline_id,
        },
        success: function (d) {
            $('#markTableDiv').html(d);
            updateButtons();
            if ($("#addGroupForm").find('input').length != 0){
                $("#close_add_group_btn").click();
            }
        },
        error: function (d) {
            console.log(d)
        }
    });
}
function changeStatus(student_id, achievement_id) {
    $.ajax({
        url: '/mainTable/changeAchievementStatus/',
        method: 'POST',
        data: {
            student_id: student_id,
            achievement_id: achievement_id,
        },
        success: function (d) {
            $('#studentAchievementsBar').html(d);
        },
        error: function (d) {
            console.log(d);
        }
    });
}

window.addEventListener("scroll", function () {
    if (
        document.body.scrollTop > 100 ||
        document.documentElement.scrollTop > 100
    ) {
        document.querySelector("#disciplines").style.marginTop = (document.querySelector("#studentAchievementsBar").offsetHeight + 10) + "px";
        document.querySelector("#studentAchievementsBar").classList.add("moved-bar");
        document.querySelector("#studentAchievementsBar").classList.remove("not-moved-bar");


    } else {
        document.querySelector("#disciplines").style.marginTop = 10 + "px";
        document.querySelector("#studentAchievementsBar").classList.remove("moved-bar");
        document.querySelector("#studentAchievementsBar").classList.add("not-moved-bar");

    }
})

function addDiscipline(e) {
    if (id_disciplineName.value) {
        document.querySelector("#disciplineSubmitButton").click();
    }
}

function openChangeBlock(event) {
    $('#editMarkBlock').css({
        display: "block",
        position: "absolute",
        left: event.pageX + "px",
        top: event.pageY + "px",
    })
    $('#editMarkBlock input').val("");
    $('#editMarkBlock input').focus();
    // event -> span -> button -> div -> td
    let cell = event.target.parentElement.parentElement.parentElement;
    let editBlock = document.querySelector("#editMarkBlock");
    editBlock.dataset.task = cell.dataset.cell_task;
    editBlock.dataset.student = cell.dataset.cell_student;
    editBlock.dataset.type = cell.dataset.type;
    editDMarkInput.addEventListener("keydown", changeMark);
}

function changeMark(event) {
    let curInput = event.target;
    let task_id = curInput.parentElement.dataset.task;
    let student_id = curInput.parentElement.dataset.student;
    let type = curInput.parentElement.dataset.type;
    if (event.keyCode === 13 && curInput.value >= 1 && curInput.value <= 5) {
        let mark = curInput.value;
        if (type === "deadline") {
            $.ajax({
                url: '/mainTable/changeMarks/',
                method: 'POST',
                data: {
                    task_id: task_id,
                    student_id: student_id,
                    knowledge_mark: 0,
                    deadline_mark: mark,
                },
                success: function (d) {
                    const data = JSON.parse(d);
                    let cell = document.querySelector(`#dlMark_${task_id}_${student_id}`);
                    let span = cell.querySelector("span");
                    span.innerHTML = data['deadlineMark'];
                    $("#dlAvgCell_" + student_id).html(data['avgDeadlineMark']);
                },
                error: function (d) {
                    console.log(d);
                }
            })
        }
        else if (type === "knowledge") {
            $.ajax({
                url: '/mainTable/changeMarks/',
                method: 'POST',
                data: {
                    task_id: task_id,
                    student_id: student_id,
                    knowledge_mark: mark,
                    deadline_mark: 0,
                },
                success: function (d) {
                    const data = JSON.parse(d);
                    let cell = document.querySelector(`#knMark_${task_id}_${student_id}`);
                    let span = cell.querySelector("span");
                    span.innerHTML = data['knowledgeMark'];
                    $("#knAvgCell_" + student_id).html(data['avgKnowledgeMark']);
                },
                error: function (d) {
                    console.log(d);
                }
            })
        }
        close_edit_mark_block();
        curInput.parentElement.removeEventListener("keydown", changeMark);
    }
}