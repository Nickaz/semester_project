let addBtn = document.querySelector("#addGroupBtn");
let input = document.querySelector("#group_name");
let form = document.querySelector("form");
addBtn.addEventListener("click", e => {
    e.preventDefault();
    if (input.value) {
        /* add group to db */
        form.submit();
        console.log("Add Btn clicked");
    }
});

function deleteGroup(name) {
    if (confirm("Вы действительно хотите удалить группу?")) {
        location.replace("deleteGroup/" + name)
    }
}