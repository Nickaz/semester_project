let addBtn = document.querySelector("#addStudentButton");
let inputFullName = document.querySelector("#full_name");
let form = document.querySelector("form");
addBtn.addEventListener("click", e => {
    e.preventDefault();
    if (
        inputFullName.value
    ) {
        /* add student to db */
        form.submit();
        console.log("Add Btn clicked");
    }
});

function deleteStudent(id) {
    if (confirm("Вы действительно хотите удалить студента?")) {
        location.replace("../deleteStudent/" + id)
    }
}