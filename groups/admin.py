from django.contrib import admin
from .models import Students, Group
# Register your models here.
admin.site.register(Students)
admin.site.register(Group)
