from django.http import HttpResponseRedirect, HttpResponseNotFound, HttpResponse
from django.contrib.auth.mixins import LoginRequiredMixin
from django.shortcuts import render, redirect
from django.views.generic import ListView
from django.views.generic.edit import FormMixin
from django.contrib.auth.decorators import login_required

from .models import Group, Students,generate_slug
from .forms import GroupForm
from achievements.models import Achievements, AchievementsList


class GroupsView(LoginRequiredMixin, FormMixin, ListView):
    model = Group
    template_name = 'groupList.html'
    context_object_name = 'groups'
    form_class = GroupForm
    success_url = '/groups/'

    def get_queryset(self):
        return Group.objects.all()

    def post(self, request, *args, **kwargs):
        form = GroupForm(request.POST)
        if form.is_valid():
            return self.form_valid(form)
        else:
            return self.form_invalid(form, request)

    def form_valid(self, form):
        group = form.save(commit=False)
        group.slug = generate_slug(group.name)
        group.save()
        return super().form_valid(form)

    def form_invalid(self, form, request):
        return render(request, self.template_name, {'form': form, 'groups': self.get_queryset()})

@login_required
def deleteGroup(request, name):
    try:
        cur_group = Group.objects.get(slug=name)
        students_of_group = Students.objects.filter(group=cur_group)
        for student in students_of_group:
            student.delete()
        cur_group.delete()
        return HttpResponseRedirect("/groups")
    except cur_group.DoesNotExist:
        return HttpResponseNotFound("<h2>Group not found</h2>")

@login_required
def editGroup(request, group):
    cur_group = Group.objects.get(slug=group)
    if request.method == 'POST':
        fullName = request.POST.get('full_name', '')
        newStudent = Students.objects.create(full_name=fullName, group=cur_group)

        for achievement in AchievementsList.objects.all():
            Achievements.objects.create(achievement_id=achievement, student_id=newStudent, status=0)
            print(Achievements.objects.all().last().achievement_id.name)

        

    students = Students.objects.filter(group=cur_group)
    return render(request, 'editGroup.html', {'students': students, "groupName": cur_group.name, })

@login_required
def deleteStudent(request, student_id):
    try:
        cur_student = Students.objects.get(id=student_id)
        cur_student.delete()
        return HttpResponseRedirect("/groups/editGroup/" + cur_student.group.slug)
    except cur_student.DoesNotExist:
        return HttpResponseNotFound("<h2>Student not found</h2>")
