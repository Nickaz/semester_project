from django import forms
import django_forms_bootstrap
from django.core.exceptions import ValidationError, ObjectDoesNotExist

from .models import Group, generate_slug

class GroupForm(forms.ModelForm):
    def clean_name(self):
        data_name = self.cleaned_data['name']
        slug = generate_slug(data_name)

        if data_name == '':
            raise forms.ValidationError('Вы оставили поле "Название" пустым')

        if len(Group.objects.filter(slug=slug)) != 0:
            raise forms.ValidationError('Такая группа уже существует')

        return data_name

    class Meta:
        model = Group
        fields = ['name',]