from django.db import models


def generate_slug(string):
    slug = string
    while slug.count('/'):
        slug = slug.replace('/', '-')
    while slug.count(' '):
        slug = slug.replace(' ', '_')
    return slug


class Group(models.Model):
    name = models.CharField(max_length=15)
    slug = models.CharField(primary_key=True, max_length=15, default=None)


class Students(models.Model):
    full_name = models.TextField()
    group = models.ForeignKey(Group, models.CASCADE, default=None)


class Discipline(models.Model):
    name = models.TextField(unique=True)


class GroupDisciplines(models.Model):
    discipline = models.ForeignKey(Discipline, models.CASCADE)
    group = models.ForeignKey(Group, models.CASCADE)
