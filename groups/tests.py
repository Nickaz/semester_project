from django.test import TestCase, Client
from django.contrib.auth.models import User

from .models import Group, Students, generate_slug

#User.objects.create_user('john', password='secret')
c = Client()


class GroupViewTest(TestCase):

    def setUp(self):
        User.objects.create_user('john', password='secret')

    def test_uses_group_list_template(self):
        self.client.login(username='john', password='secret')
        response = self.client.get('/groups/')
        self.assertTemplateUsed(response, 'groupList.html')

    def test_uses_group_edit_template_for_adding_students(self):
        self.client.login(username='john', password='secret')
        new_group = Group.objects.create(name='KI-18', slug='KI-18')

        response = self.client.get(f'/groups/editGroup/{new_group.slug}/')

        self.assertTemplateUsed(response, 'editGroup.html')

    def test_displays_only_students_for_that_group(self):
        self.client.login(username='john', password='secret')
        first_group = Group.objects.create(name="first", slug='first')
        second_group = Group.objects.create(name="second", slug='second')

        f_student = Students.objects.create(full_name="Alala", group=first_group)
        s_student = Students.objects.create(full_name="Balaba", group=second_group)

        response = self.client.get(f'/groups/editGroup/{first_group.slug}/')

        self.assertContains(response, f_student.full_name)
        self.assertNotContains(response, s_student.full_name)
