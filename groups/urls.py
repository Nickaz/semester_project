from django.urls import path
from . import views
from .views import GroupsView

urlpatterns = [
    path('', GroupsView.as_view(), name='groupView'),
    path('editGroup/<str:group>/', views.editGroup, name='editGroup'),
    path('deleteGroup/<str:name>/', views.deleteGroup, name='deleteGroup'),
    path('editGroup/deleteStudent/<int:student_id>/', views.deleteStudent, name='deleteStudent'),
]
