from django.urls import path
from table import views
from table import ajax

urlpatterns = [
    path('', views.MainTableView.as_view(), name='mainTable'),
    path('addMark/', ajax.addMark, name='addMark'),
    path('addTask/', ajax.addTask, name='addTask'),
    path('studentAchievements/', ajax.studentAchievements, name='studentAchievements'),
    path('chooseDiscipline/', ajax.chooseDiscipline, name='chooseDiscipline'),
    path('addDiscipline/', ajax.addDiscipline, name='addDiscipline'),
    path('changeAchievementStatus/', ajax.changeAchievementStatus, name='changeAchievementStatus'),
    path('getGroupsForDiscipline/', ajax.getGroupsForDiscipline, name='getGroupsForDiscipline'),
    path('addGroupToDiscipline/', ajax.addGroupToDiscipline, name='addGroupToDiscipline'),
    path('addKnowledgeMark/', ajax.addKnowledgeMark, name='addKnowledgeMark'),
    path('changeMarks/', ajax.changeMarks, name='changeMarks'),
    path('deleteTask/', ajax.deleteTask, name='deleteTask'),
]