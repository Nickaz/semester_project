from django.test import TestCase
from django.contrib.auth.models import User
from groups.models import Students, Group
from achievements.models import AchievementsList


class TableViewTest(TestCase):

    def setUp(self):
        User.objects.create_user('john', password='secret')

    def test_uses_main_table_template(self):
        self.client.login(username='john', password='secret')
        response = self.client.get('/mainTable/')
        self.assertTemplateUsed(response, 'mainTable.html')

    def test_table_saves_all_new_students(self):
        self.client.login(username='john', password='secret')
        correct_group = Group.objects.create(name='КИ-18', slug='КИ-18')
        Students.objects.create(full_name='ПетровГЕ', group=correct_group)
        Students.objects.create(full_name='ИвановАК', group=correct_group)

        self.assertEqual(Students.objects.count(), 2)

