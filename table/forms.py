from django import forms

from groups.models import Group
from table.models import Tasks


class TaskForm(forms.ModelForm):
    deadline = forms.DateField(label='Дедлайн', required=True,
                               input_formats=['%Y-%m-%d', '%d.%m.%Y'],
                               widget=forms.SelectDateWidget)
    name = forms.CharField(label='Название')

    class Meta:
        model = Tasks
        fields = ['name', 'deadline']


class MarkForm(forms.Form):
    task = forms.CharField(label='task_id')
    mark = forms.IntegerField(label='Оценка', max_value=5, min_value=1)
    student_id = forms.CharField(label='student_id')
    comment = forms.CharField(label='Коммент', widget=forms.Textarea(attrs={"rows": 3}))


class DisciplineForm(forms.Form):
    disciplineName = forms.CharField(label='')


class GroupsForDiscipline(forms.Form):
    groups = forms.MultipleChoiceField()
    deadline_id = forms.IntegerField(label='')

    def __init__(self, *args, group_list, **kwargs):
        super(GroupsForDiscipline, self).__init__(*args, **kwargs)
        self.fields['groups'].choices = group_list
