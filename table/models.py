from django.db import models
from groups.models import Students, Discipline


class Tasks(models.Model):
    name = models.CharField(max_length=30)
    deadline = models.DateField()
    discipline = models.ForeignKey(Discipline, models.CASCADE)


class Marks(models.Model):
    task = models.ForeignKey(Tasks, on_delete=models.CASCADE)
    student_id = models.ForeignKey(Students, on_delete=models.CASCADE)
    deadline_mark = models.IntegerField()
    knowledge_mark = models.IntegerField()
    comment = models.TextField()
