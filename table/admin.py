from django.contrib import admin
from .models import Tasks, Marks
# Register your models here.

admin.site.register(Tasks)
admin.site.register(Marks)
