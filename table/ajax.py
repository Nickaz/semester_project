import datetime
import json

from django.core.exceptions import ObjectDoesNotExist
from django.http import HttpResponse, HttpResponseBadRequest
from django.shortcuts import render
from django import forms

from achievements.models import Achievements, AchievementsList
from table.forms import TaskForm, DisciplineForm, GroupsForDiscipline
from table.models import Marks, Tasks
from groups.models import Students, Discipline, GroupDisciplines, Group
from table.views import MainTableView, get_group_list


def getAvgMarks(student, discipline_tasks):
    marks = Marks.objects.filter(student_id=student, task__in=discipline_tasks).order_by('task')
    average_deadline_mark = 0
    average_knowledge_mark = 0
    for mark in marks:
        average_deadline_mark += mark.deadline_mark
        average_knowledge_mark += mark.knowledge_mark
    average_deadline_mark = average_deadline_mark / len(marks)
    average_knowledge_mark = average_knowledge_mark / len(marks)
    return average_deadline_mark, average_knowledge_mark


def renderInfo(discipline):
    tasks = Tasks.objects.filter(discipline=discipline)
    groups = GroupDisciplines.objects.filter(discipline=discipline)
    groups__names = []
    for group in groups:
        groups__names.append(group.group.slug)
    students = Students.objects.filter(group__in=groups__names)
    studentsMarks = MainTableView.get_studentsMarks(MainTableView, students, tasks)
    return {'studentsMarks': studentsMarks,
            'tasks': tasks,
            'marks': Marks.objects.all(),
            'discipline_id': discipline.id,
            }


def addMark(request):
    task = Tasks.objects.get(id=(request.POST['task_id']))
    discipline_tasks = Tasks.objects.filter(discipline=task.discipline)
    student = Students.objects.get(id=(request.POST['student_id']))
    now = datetime.date.today()
    deadline = task.deadline
    difference = now - deadline
    if now < deadline:
        mark = 5
    elif now > deadline and (difference.days < 7):
        mark = 4
    elif now > deadline and (difference.days < 14):
        mark = 3
    elif now > deadline and (difference.days < 21):
        mark = 2
    else:
        mark = 1
    Marks.objects.create(task=task, student_id=student, deadline_mark=mark, knowledge_mark=0, comment='')
    average_deadline_mark, _ = getAvgMarks(student, discipline_tasks)
    return HttpResponse(json.dumps({'deadlineMark': mark, 'avgDeadlineMark': average_deadline_mark}))


def addKnowledgeMark(request):
    task = Tasks.objects.get(id=(request.POST['task_id']))
    discipline_tasks = Tasks.objects.filter(discipline=task.discipline)
    student = Students.objects.get(id=(request.POST['student_id']))
    mark = request.POST['mark']
    markObj = Marks.objects.get(task=task, student_id=student)
    markObj.knowledge_mark = mark
    markObj.save()
    _, average_knowledge_mark = getAvgMarks(student, discipline_tasks)
    return HttpResponse(json.dumps({'knowledgeMark': mark, 'avgKnowledgeMark': average_knowledge_mark}))


def changeMarks(request):
    try:
        task = Tasks.objects.get(id=request.POST['task_id'])
        discipline_tasks = Tasks.objects.filter(discipline=task.discipline)
        student = Students.objects.get(id=request.POST['student_id'])
        mark = Marks.objects.get(task=task, student_id=student)
        if request.POST['deadline_mark'] == '0':
            mark.knowledge_mark = request.POST['knowledge_mark']
        elif request.POST['knowledge_mark'] == '0':
            mark.deadline_mark = request.POST['deadline_mark']
        mark.save()
        average_deadline_mark, average_knowledge_mark = getAvgMarks(student, discipline_tasks)
        return HttpResponse(json.dumps({'knowledgeMark': mark.knowledge_mark,
                                        'avgKnowledgeMark': average_knowledge_mark,
                                        'deadlineMark': mark.deadline_mark,
                                        'avgDeadlineMark': average_deadline_mark}))
    except:
        return HttpResponseBadRequest("error")


def addTask(request):
    print(request.POST)
    try:
        discipline = Discipline.objects.get(id=request.POST['discipline_id'])
        task_name = request.POST['name']
        deadline_day = int(request.POST['deadline_day'])
        deadline_month = int(request.POST['deadline_month'])
        deadline_year = int(request.POST['deadline_year'])
        deadline = datetime.date(deadline_year, deadline_month, deadline_day)
        Tasks.objects.create(name=task_name,
                             deadline=deadline,
                             discipline=discipline)
        return render(request, "table.html", renderInfo(discipline=discipline))
    except:
        return HttpResponseBadRequest("error")


def deleteTask(request):
    try:
        discipline = Discipline.objects.get(id=request.POST['discipline_id'])
        print(request.POST['id'])
        task = Tasks.objects.get(id=request.POST['id'])
        task.delete()
        return render(request, 'table.html', renderInfo(discipline=discipline))
    except Tasks.DoesNotExist:
        return HttpResponse("error")


def studentAchievements(request):
    student_id = Students.objects.get(id=request.POST['student_id'])
    achievements = Achievements.objects.filter(student_id=student_id)
    return render(request, "studentAchievementsBar.html", {'studentAchievements': achievements})


def changeAchievementStatus(request):
    student = Students.objects.get(id=request.POST['student_id'])
    achievement = AchievementsList.objects.get(id=request.POST['achievement_id'])
    cur_achievement = Achievements.objects.get(student_id=student, achievement_id=achievement)
    if cur_achievement.status != 2:
        cur_achievement.status += 1
    else:
        cur_achievement.status = 0
    cur_achievement.save()
    achievements = Achievements.objects.filter(student_id=student)
    return render(request, "studentAchievementsBar.html", {'studentAchievements': achievements})


def chooseDiscipline(request):
    discipline = Discipline.objects.get(id=request.POST['discipline_id'])
    return render(request, "table.html", renderInfo(discipline=discipline))


def addDiscipline(request):
    form = DisciplineForm(request.POST)
    if form.is_valid():
        discipline_name = form.cleaned_data["disciplineName"]
        Discipline.objects.create(name=discipline_name)
        return render(request, "subjectsTable.html", {'disciplines': Discipline.objects.all(),
                                                      'disciplineForm': DisciplineForm})
    else:
        return HttpResponse("error")


def getArrayOfGroupsForDiscipline(discipline):
    groups_for_discipline = Group.objects.all(). \
        exclude(name__in=[line.group.name
                          for line
                          in GroupDisciplines.objects.filter(discipline=discipline)])
    return [(group.name, group.name) for group in groups_for_discipline]


def addGroupToDiscipline(request):
    try:
        discipline = Discipline.objects.get(id=request.POST['deadline_id'])
        for group_name in request.POST.getlist('groups'):
            group = Group.objects.get(name=group_name)
            GroupDisciplines.objects.create(group=group, discipline=discipline)
        return render(request, "table.html", renderInfo(discipline=discipline))
    except:
        return HttpResponseBadRequest('error')


def getGroupsForDiscipline(request):
    discipline = request.POST['discipline_id']
    arrayOfGroupsForDiscipline = getArrayOfGroupsForDiscipline(discipline)
    if len(arrayOfGroupsForDiscipline) == 0:
        return HttpResponseBadRequest("Все группы добавлены")
    groups_multiple_choice_form = GroupsForDiscipline(group_list=arrayOfGroupsForDiscipline)
    return HttpResponse(groups_multiple_choice_form)
