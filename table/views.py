import copy

from django.db.models import QuerySet
from django.http import HttpResponseRedirect
from django.shortcuts import render, redirect
from django.views.generic import TemplateView
from django.views.generic.edit import FormMixin
from django.contrib.auth.mixins import LoginRequiredMixin

from groups.models import Students, Discipline, GroupDisciplines, Group
from achievements.models import Achievements, AchievementsList
from table.models import Marks, Tasks
from table.forms import TaskForm, MarkForm, DisciplineForm


class MainTableView(LoginRequiredMixin, FormMixin, TemplateView):
    template_name = 'mainTable.html'
    form_class = TaskForm
    second_form_class = MarkForm
    third_form_class = DisciplineForm
    success_url = "."

    def get_studentsMarks(self, students, discipline_tasks):
        studentMarks = {}
        marks = []
        tasks = []
        average_knowledge_mark = 0
        average_deadline_mark = 0
        for i in range(len(students)):
            cur_marks = Marks.objects.filter(student_id=students[i], task__in=discipline_tasks).order_by('task')
            for idx in range(len(cur_marks)):
                average_deadline_mark += cur_marks[idx].deadline_mark
                average_knowledge_mark += cur_marks[idx].knowledge_mark
                marks.append(cur_marks[idx])

            for idx in range(len(cur_marks)):
                tasks.append(cur_marks[idx].task)
            idx = 0
            for task in discipline_tasks.order_by('id'):
                if task not in tasks:
                    marks = marks[:idx] + [task] + marks[idx:]
                idx += 1
            if len(cur_marks):
                average_deadline_mark = average_deadline_mark / len(cur_marks)
                average_knowledge_mark = average_knowledge_mark / len(cur_marks)
            marks.append((average_deadline_mark, average_knowledge_mark))
            studentMarks.update([(students[i], copy.deepcopy(marks))])
            marks.clear()
            tasks.clear()
            average_deadline_mark = 0
            average_knowledge_mark = 0
        return studentMarks

    def get_context_data(self, **kwargs):
        # все принты чисто для меня, чтобы видеть что да как, потом уберем
        context = super().get_context_data(**kwargs)
        students = Students.objects.all()

        context['students'] = students

        context['studentsMarks'] = self.get_studentsMarks(students, Tasks.objects.all())

        # Тест
        # { % for student in studentsMarks %}
        # {{ student.0.student_id.full_name }}
        # { % for mark in student % }
        # < p > Задание: {{mark.task}} < / p >
        # < p > Студент: {{mark.student_id.id}} < / p >
        # < p > Оценка: {{mark.mark}} < / p >
        # < p > Коммент: {{mark.comment}} < / p >
        # { % endfor %}
        # { % endfor %}

        context['achievements'] = AchievementsList.objects.all()

        context['marks'] = Marks.objects.all()

        context['tasks'] = Tasks.objects.all()

        context['taskForm'] = self.form_class()
        context['markForm'] = self.second_form_class()
        context['disciplineForm'] = self.third_form_class()

        context['disciplines'] = Discipline.objects.all()
        return context

    def post(self, request, *args, **kwargs):
        if "taskSubmitButton" in request.POST:
            form = self.form_class(request.POST)
            if form.is_valid():
                return self.form_valid(form)
            else:
                return self.form_invalid(form)
        elif "markSubmitButton" in request.POST:
            form = self.second_form_class(request.POST)
            if form.is_valid():
                task_id = form.cleaned_data['task']
                task = Tasks.objects.get(id=task_id)
                student_id = form.cleaned_data['student_id']
                student = Students.objects.get(id=student_id)
                mark = form.cleaned_data['mark']
                comment = form.cleaned_data['comment']
                Marks.objects.create(task=task, student_id=student, mark=mark, comment=comment)
                return HttpResponseRedirect('.')
            else:
                return self.form_invalid()

    def form_valid(self, form):
        form.save()
        return super().form_valid(form)


def get_group_list():
    Groups = Group.objects.all()
    groups_list = []
    for group in Groups:
        groups_list.append((group.name, group.name))
    return groups_list


def homePage(request):
    return redirect('/mainTable/')
